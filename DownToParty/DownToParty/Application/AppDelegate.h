//
//  AppDelegate.h
//  DownToParty
//
//  Created by victory on 4/7/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserEntity.h"
#import "MBProgressHUD.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    
    UserEntity *Me;
    
    MBProgressHUD * HUD;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) UserEntity *Me;

- (void) showAlertDialog : (NSString *)title message:(NSString *) message positive:(NSString *)strPositivie negative:(NSString *) strNegative sender:(id) sender;

// check the current network state
-(BOOL) connected;

// show loading state when we connect to server
- (void) showLoadingViewWithTitle:(NSString *) title sender:(id) sender;

// hide loading view
-(void) hideLoadingView;
- (void) hideLoadingView : (NSTimeInterval) delay;

@end

