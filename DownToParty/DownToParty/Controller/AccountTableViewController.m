//
//  AccountTableViewController.m
//  DownToParty
//
//  Created by victory on 4/9/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "AccountTableViewController.h"
#import "UserEntity.h"
#import "ReqConst.h"
#import "CommonUtils.h"
#import "NSString+Encode.h"

@interface AccountTableViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate> {
    
    UserEntity *_user;
    
    NSString *strPhotoPath;
}

@property (nonatomic, weak) IBOutlet UIImageView *imvProfile;
@property (nonatomic, weak) IBOutlet UITextField *txtName;
@property (nonatomic, weak) IBOutlet UISwitch *swAllowPush;
@property (nonatomic, weak) IBOutlet UISwitch *swAllowLocation;
@end

@implementation AccountTableViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self initVars];
    
    [self initView];
}

- (void) initVars {
    
    _user = APPDELEGATE.Me;
}

- (void) initView {
    
    self.navigationItem.hidesBackButton = YES;
    
    if(_user._name != nil) {
        
        _txtName.text = _user._name;
    }
    
    if(_user._photoUrl != nil && ![_user._photoUrl isEqualToString: @""]) {
        
        strPhotoPath = _user._photoUrl;
        
        [_imvProfile setImageWithURL:[NSURL URLWithString:_user._photoUrl] placeholderImage:[UIImage imageNamed:@"test1"]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 5;
}

- (IBAction) addProfilePic:(id)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self openCamera];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self openGallery];
    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    actionSheet.view.tintColor = [UIColor lightGrayColor];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void) openCamera {
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    } else {
        
        NSLog(@"No Cameran\n. Please test on device");
    }
}

- (void) openGallery {
    
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController: imagePicker animated:YES completion:nil];
}

#pragma mark -
#pragma mark - UIImagePickerControllerDelegate

// This method is called when an image has been chosen from the album or taken from the camera
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage * chosenImage = info[UIImagePickerControllerEditedImage];
    
    self.imvProfile.image = chosenImage;
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
    [picker dismissViewControllerAnimated:YES completion:^{
        
            dispatch_queue_t writeQueue = dispatch_queue_create("SavePhoto", NULL);
    
            dispatch_async(writeQueue, ^{
    
                NSString * photoPath = [CommonUtils saveToFile:chosenImage isProfile:YES];
    
                dispatch_async(dispatch_get_main_queue(), ^ {
    
                    strPhotoPath = photoPath;
    
                    // update ui (set profile image with saved Photo URL
                    [_imvProfile setImage:[UIImage imageWithContentsOfFile:strPhotoPath]];
                });
            });
    }];
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITextFieldDelegate
- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    return YES;
}

- (void) uploadProfileImage {
    
    [APPDELEGATE showLoadingViewWithTitle:nil sender:self];
    
    // upload profile image
    NSString * url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_PROFILE_PHOTO];
    NSDictionary *params = @{
                             PARAM_ID : [NSNumber numberWithInt:_user._idx]
                             };
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:[NSURL fileURLWithPath:strPhotoPath] name:@"file" fileName:@"filename.jpg" mimeType:@"image/jpg" error:nil];
    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:nil
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      
                      if (error) {
                          
                          NSLog(@"Error: %@", error);
                          
                          [APPDELEGATE hideLoadingView];
                          [APPDELEGATE showAlertDialog:nil message:PHOTO_UPLOAD_FAIL positive:ALERT_OK negative:nil sender:self];
                          
                      } else {
                          
                          NSLog(@"%@ %@", response, responseObject);
                          
                          int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
                          
                          if(nResultCode == 0) {
                              
                              [self registerUser];
                              
                          } else {
                              
                              [APPDELEGATE hideLoadingView];
                              [APPDELEGATE showAlertDialog:nil message:PHOTO_UPLOAD_FAIL positive:ALERT_OK negative:nil sender:self];
                          }
                      }
                  }];
    
    [uploadTask resume];
}

- (void) registerUser {
    
    NSString *strUserName = [[_txtName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] encodeString:NSUTF8StringEncoding];
    
    // make server url
    NSString * url = [NSString stringWithFormat:@"%@%@/%d/%@", SERVER_URL, REQ_REGISTER, _user._idx, strUserName];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        [APPDELEGATE hideLoadingView];
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            NSDictionary *dict = [responseObject objectForKey:RES_USERINFO];
            
            _user._name = [dict valueForKey:RES_NAME];
            _user._photoUrl = [dict valueForKey:RES_PHOTOURL];
            
            [self onSuccessRegister];
            
        } else {
            
            [[JLToast makeText:REGISTER_FAIL duration: 3] show];
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        [APPDELEGATE hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        
        NSLog(@"Error: %@", error);
    }];
}

- (IBAction) setProfile:(id)sender {
    
    if(_txtName.text.length == 0) {
        
        [APPDELEGATE showAlertDialog:nil message:INPUT_FULL_NAME positive:ALERT_OK negative:nil sender:self];
        return;
    } else if(strPhotoPath == nil || [strPhotoPath isEqualToString:@""]) {
        
        [APPDELEGATE showAlertDialog:nil message:SELECT_PHOTO positive:ALERT_OK negative:nil sender:self];
        return;
    }
    
    [self uploadProfileImage];
}

- (void) onSuccessRegister {
    
    _user._isAllowedLocService = self.swAllowLocation.isOn;
    _user._isAllowedPush = self.swAllowPush.isOn;
    
    [CommonUtils saveUserInfo];
    
    [CommonUtils setUserLogin:YES];
    
    UINavigationController *homeNav = (UINavigationController *) [self.storyboard instantiateViewControllerWithIdentifier:@"MainNav"];
    
    [[UIApplication sharedApplication].keyWindow setRootViewController:homeNav];
}


@end
