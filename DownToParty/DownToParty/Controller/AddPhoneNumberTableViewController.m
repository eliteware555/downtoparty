//
//  AddPhoneNumberTableViewController.m
//  DownToParty
//
//  Created by victory on 4/9/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "AddPhoneNumberTableViewController.h"
#import "CountryListViewController.h"
#import "UserEntity.h"
#import "CommonUtils.h"
#import "ViewUtils.h"
#import "VMaskTextField.h"
#import "NSString+Encode.h"
#import "ReqConst.h"

@interface AddPhoneNumberTableViewController () <CountryListViewDelegate, UITextFieldDelegate> {

    NSString *countryCode;
    UserEntity *_user;
    
    UIActivityIndicatorView *activityIndicator;
    
    NSString *phoneNumber;
}

@property (nonatomic, weak) IBOutlet UILabel *lblCountryName;

@property (nonatomic, weak) IBOutlet UITextField *txtDialCode;
@property (nonatomic, weak) IBOutlet VMaskTextField *txtPhoneNumber;

@end

@implementation AddPhoneNumberTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self initVars];
    
    [self initView];
}

- (void) initVars {
    
    _user = APPDELEGATE.Me;
    
    countryCode = _user._countryCode;
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [activityIndicator setColor:[UIColor darkGrayColor]];
    activityIndicator.hidesWhenStopped = YES;
}

- (void) initView {
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.lblCountryName.text = _user._countryName;
    self.txtDialCode.text = _user._dialCode;
    
    self.txtPhoneNumber.mask = @"### #### ####";
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [_txtPhoneNumber becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
 
    VMaskTextField * maskTextField = (VMaskTextField*) textField;
    
    // set navigation title with inputed phone number
   
    if(textField.text.length == 1 && [string isEqualToString:@""]) {
        
        self.navigationItem.title = self.txtDialCode.text;
    } else if (textField.text.length >= 13) {
    
        self.navigationItem.title = [NSString stringWithFormat:@"%@ %@", self.txtDialCode.text, textField.text];
        
    } else {
        
        self.navigationItem.title = [NSString stringWithFormat:@"%@ %@%@", self.txtDialCode.text, textField.text, string];
    }
    

    return  [maskTextField shouldChangeCharactersInRange:range replacementString:string];
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 3;
}

- (void) didSelectCountry:(NSDictionary *)country {
    
    _lblCountryName.text = [country valueForKey:@"name"];
    _txtDialCode.text = [country valueForKey:@"dial_code"];
    
    countryCode = [country valueForKey:@"code"];
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}

- (void) showConfirmAlert {
    
    NSString *message = [NSString stringWithFormat:@"\n%@\n\n%@", phoneNumber, ALERT_MESSAGE_PHONENUMBER];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:ALERT_CONFIRM_PHONENUMBER message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * editAction = [UIAlertAction
                                 actionWithTitle:ALERT_EDIT
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     //Handel your yes please button action here
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                     [self.txtPhoneNumber becomeFirstResponder];
                                 }];
    
    [alert addAction:editAction];
    
    UIAlertAction * confirmAction = [UIAlertAction
                                  actionWithTitle:ALERT_YES
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      //Handel your yes please button action here
                                      [alert dismissViewControllerAnimated:YES completion:nil];
                                      
                                      [self getAuthCode];
                                  }];
    
    [alert addAction:confirmAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void) showLoadingView:(NSString *) title {
    
    UIView *maskView = [[UIView alloc] initWithFrame:CGRectMake(0, -20, [[UIScreen mainScreen] applicationFrame].size.width, [[UIScreen mainScreen] applicationFrame].size.height + 20.0f)];
    [maskView setBackgroundColor:[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:0.90]];
    maskView.userInteractionEnabled = YES;
    maskView.tag = 100;
    
    [self.navigationController.navigationBar addSubview:maskView];
    [self.navigationController.navigationBar bringSubviewToFront:maskView];
    
    [maskView addSubview:activityIndicator];
    activityIndicator.center = maskView.contentCenter;
    
    UILabel *lblConnecting = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, maskView.bounds.size.width, 30.0f)];
    lblConnecting.textColor = [UIColor darkGrayColor];
    lblConnecting.font = [UIFont systemFontOfSize:20.0f];
    lblConnecting.text = title;
    lblConnecting.textAlignment = NSTextAlignmentCenter;
    [maskView addSubview:lblConnecting];
    
    lblConnecting.bottom = maskView.bounds.size.height / 2.0 + 50.0f;
    lblConnecting.tag = 101;
    
    [activityIndicator startAnimating];
}

- (void) hideLoadingView {
    
    [activityIndicator stopAnimating];
    [[self.navigationController.navigationBar viewWithTag:100] removeFromSuperview];
}

- (IBAction) doneAction:(id) sender {
    
    // for a test
    [self performSegueWithIdentifier:@"Segue2VierifyCode" sender:self];
    
//    if ([self checkValid]) {
//        
//        [self.view endEditing:YES];
//    
//        phoneNumber = [[NSString stringWithFormat:@"%@ %@", _txtDialCode.text, _txtPhoneNumber.text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//        
//        [self registerPhoneNumber];
//    }
}


- (BOOL) checkValid {
    
    if (self.txtDialCode.text.length == 0) {
        
        [APPDELEGATE showAlertDialog:nil message:INPUT_DIALCODE positive:ALERT_OK negative:nil sender:self];
        return NO;
    } else if (self.txtPhoneNumber.text.length == 0) {
        
        [APPDELEGATE showAlertDialog:nil message:INPUT_PHONENUMBER positive:ALERT_OK negative:nil sender:self];
        return NO;
    }
    
    return YES;
}

- (void) registerPhoneNumber {
    
    [self showLoadingView:LOADING];
    
    NSString *strPhoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    strPhoneNumber = [strPhoneNumber encodeString:NSUTF8StringEncoding];
    
    // make server url
    NSString * url = [NSString stringWithFormat:@"%@%@/%@", SERVER_URL, REQ_REGISTER_PHONENUMBER, strPhoneNumber];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        [self hideLoadingView];
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            // get user id
            _user._idx = [[responseObject valueForKey:RES_ID] intValue];
            
            // show alert screen to confirm input value
            [self showConfirmAlert];
            
        } else {
        
            [[JLToast makeText:CONN_ERROR duration: 3] show];
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        [self hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        
        NSLog(@"Error: %@", error);
    }];
}

- (void) getAuthCode {
    
    [self showLoadingView:[NSString stringWithFormat:@"%@ %@", VERIFYING, phoneNumber]];
    
    NSString *strPhoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    strPhoneNumber = [strPhoneNumber encodeString:NSUTF8StringEncoding];
    
    // make server url
    NSString * url = [NSString stringWithFormat:@"%@%@/%d/%@", SERVER_URL, REQ_GETAUTHCODE, _user._idx, strPhoneNumber];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        [self hideLoadingView];
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            // goto verify viewcontroller
            [self gotoVerify];
            
        } else {
            
            [[JLToast makeText:WRONG_PHONENUMBER duration: 3] show];
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        [self hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        
        NSLog(@"Error: %@", error);
    }];
}

- (void) setUserInfo {
    
    // dial code, country name, country code, phone number
    _user._dialCode = [_txtDialCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _user._countryName = _lblCountryName.text;
    _user._countryCode = countryCode;
    _user._phoneNumber = [_txtPhoneNumber.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (void) gotoVerify {
    
    [self setUserInfo];
    
    [self performSegueWithIdentifier:@"Segue2VierifyCode" sender:self];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"Segue2SelectCountry"]) {
        
        CountryListViewController *countryListVC = (CountryListViewController *) [segue destinationViewController];
        countryListVC.delegate = self;
        countryListVC.countryCode = countryCode;
    }
}


@end
