//
//  ChatListViewController.m
//  DownToParty
//
//  Created by victory on 4/9/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "ChatListViewController.h"
#import "ChatListCell.h"
#import "RoomEntity.h"

@interface ChatListViewController () <UITableViewDataSource, UITableViewDelegate> {
    
    
}

@property (nonatomic, weak) IBOutlet UITableView *tblChatList;

@end

@implementation ChatListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initVars];
    
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initVars {
    
}

- (void) initView {
    
    // remove cell seperater for empty cells
    self.tblChatList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

#pragma makr -
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 6;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"ChatListCell";
    
    ChatListCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    RoomEntity *_chatRoom = [[RoomEntity alloc] init];

    switch (indexPath.row) {
        case 0:
            
            _chatRoom._displayName = @"pearlgill";
            _chatRoom._currentUsers = 2;
            _chatRoom._recentCounter = 0;
            break;
            
        case 1:
            
            _chatRoom._displayName = @"misshollytaylor, butcherboytom, santiagom27, pearlgill";
            _chatRoom._currentUsers = 5;
            _chatRoom._recentCounter = 3;
            break;
            
        case 2:
            
            _chatRoom._displayName = @"butcherboytom, santiagom27, pearlgill";
            _chatRoom._currentUsers = 4;
            _chatRoom._recentCounter = 1;
            break;
            
        case 3:
            
            _chatRoom._displayName = @"santiagom27, mister_keating";
            _chatRoom._currentUsers = 3;
            _chatRoom._recentCounter = 2;
            
            break;
            
        case 4:
            
            _chatRoom._displayName = @"mister_keating";
            _chatRoom._currentUsers = 2;
            _chatRoom._recentCounter = 2;
            break;
            
        case 5:
            
            _chatRoom._displayName = @"misshollytaylor";
            _chatRoom._currentUsers = 2;
            _chatRoom._recentCounter = 9;
            break;
        default:
            break;
    }
    
    [cell setRoom:_chatRoom];
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 68.0;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self performSegueWithIdentifier:@"SegueChatList2Chat" sender:self];
}


- (IBAction) addChatAction:(id)sender {
    
}

- (IBAction) backwardAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
