//
//  ChatViewController.m
//  DownToParty
//
//  Created by victory on 4/9/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "ChatViewController.h"
#import "Inputbar.h"

@interface ChatViewController ()

@property (nonatomic, weak) IBOutlet Inputbar *inputbar;

@end

@implementation ChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initVars];
    
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initVars {
    
}

- (void) initView {
    
    // navigation title
    int nFriendCount = 2;
    NSString *navTitleFriend = @"Group Chat ";
    NSString *navTitleFriendCount = [NSString stringWithFormat:@"%i", nFriendCount];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@", navTitleFriend, navTitleFriendCount]];
    NSDictionary *whiteBoldArial = @{NSForegroundColorAttributeName :[UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"Arial-BoldMT" size:20.0]};
    NSDictionary *yellowBoldHelvetica = @{NSForegroundColorAttributeName :[UIColor colorWithRed:255/255.0 green:242/255.0 blue:0/255.0 alpha:1.0], NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:20.0]};
    
    [attributedString addAttributes:whiteBoldArial range:NSMakeRange(0, navTitleFriend.length)];
    [attributedString addAttributes:yellowBoldHelvetica range:NSMakeRange(navTitleFriend.length+1, navTitleFriendCount.length)];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 21)];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    [lblTitle setAttributedText:attributedString];
    
    self.navigationItem.titleView = lblTitle;
    
    [self setupInputbar];
}

- (void) setupInputbar {
    
    self.inputbar.rightButtonImage = [UIImage imageNamed:@"send"];
    self.inputbar.leftButtonImage = [UIImage imageNamed:@"take_picture"];
    self.inputbar.voiceButtonImage = [UIImage imageNamed:@"voice_record"];
}

- (IBAction) backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
