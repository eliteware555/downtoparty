//
//  CountryListDataSource.m
//  Country List
//
//  Created by Pradyumna Doddala on 18/12/13.
//  Copyright (c) 2013 Pradyumna Doddala. All rights reserved.
//

#import "CountryListDataSource.h"

#define kCountriesFileName @"countries.json"

@interface CountryListDataSource () {
    
    NSArray *countriesList;
}

@end

@implementation CountryListDataSource

- (id)init {
    
    if (self = [super init]) {
        
        [self parseJSON];
    }
    
    return self;
}

- (void)parseJSON {
    
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"countries" ofType:@"json"]];
    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    
    if (localError != nil) {
        NSLog(@"%@", [localError userInfo]);
    }
    countriesList = (NSArray *)parsedObject;
}

// tableview datasource
- (NSArray *) countries
{
    return countriesList;
}

// sort and make section
- (NSMutableArray *) getCountryListData {
    
    NSMutableArray *ans = [[NSMutableArray alloc] init];
    
    // sort array
    NSArray *serializeArray = [(NSArray *)countriesList sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        
        int i;
        
        NSString *strA =  [obj1 valueForKey:kCountryName];
        NSString *strB = [obj2 valueForKey:kCountryName];
        
        for (i = 0; i < strA.length && i < strB.length; i ++) {
            
            char a = [strA characterAtIndex:i];
            char b = [strB characterAtIndex:i];
            
            if (a > b) {
                
                return (NSComparisonResult) NSOrderedDescending; //increase
            }
            else if (a < b) {
                
                return (NSComparisonResult) NSOrderedAscending; //decrease
            }
        }
        
        if (strA.length > strB.length) {
            
            return (NSComparisonResult)NSOrderedDescending;
            
        }else if (strA.length < strB.length){
            
            return (NSComparisonResult)NSOrderedAscending;
            
        }else{
            
            return (NSComparisonResult)NSOrderedSame;
        }
    }];
    
    
    // make msmutablearray with array
    
    char lastC = '1';
    
    NSMutableArray *data;
    NSMutableArray *oth = [[NSMutableArray alloc] init];
    
    for (NSDictionary *country in serializeArray) {
        
        char c = [[country valueForKey:kCountryName] characterAtIndex:0];
        
        // check wheter a char is digit or not
        // not char
        if (!isalpha(c)) {
            
            [oth addObject:country];
            
        }
        
        // change section
        else if (c != lastC){
            
            lastC = c;
            
            if (data && data.count > 0) {
                
                [ans addObject:data];
            }
            
            data = [[NSMutableArray alloc] init];
            [data addObject:country];
            
        }
        
        // add object in the same section
        else {
            
            [data addObject:country];
        }
    }
    
    if (data && data.count > 0) {
        [ans addObject:data];
    }
    
    if (oth.count > 0) {
        
        [ans addObject:oth];
    }

    return ans;
}

// return section
- (NSMutableArray *) getCountryListSectionBy: (NSMutableArray *) array {
    
    NSMutableArray *section = [[NSMutableArray alloc] init];

    for (NSArray *item in array) {
        
        NSDictionary *country = [item objectAtIndex:0];
        
        char c = [[country valueForKey:kCountryName] characterAtIndex:0];
        
        if (!isalpha(c)) {
            c = '#';
        }
        
        [section addObject:[NSString stringWithFormat:@"%c", toupper(c)]];
    }
    
    return section;
}


@end
