//
//  CountryListViewController.m
//  Country List
//
//  Created by Pradyumna Doddala on 18/12/13.
//  Copyright (c) 2013 Pradyumna Doddala. All rights reserved.
//

#import "CountryListViewController.h"
#import "CountryListDataSource.h"
#import "CountryCell.h"
#import "CommonUtils.h"

@interface CountryListViewController () <UISearchBarDelegate> {
    
    NSIndexPath *selectedIndex;
    
    // for searching
    BOOL searchActivie;
    
    NSMutableArray *filteredCountriesList;
   
    NSMutableArray *dataRows;              // row array
    NSArray *sectionList;    // section array
}


@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, weak) IBOutlet UISearchBar *searchField;

@end

@implementation CountryListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // get country list
    [self initVars];
    
    // load table data
    [self initView];
}

- (void) initVars {
    
    searchActivie = NO;
    
    CountryListDataSource *dataSource = [[CountryListDataSource alloc] init];
    dataRows = [dataSource getCountryListData];
    
    sectionList = [dataSource getCountryListSectionBy:dataRows];
    
    filteredCountriesList = [[NSMutableArray alloc] init];
    
    selectedIndex = nil;
    
    int row = 0;
    for (NSArray *array in dataRows) {
        
        for( int j = 0; j < [array count]; j ++) {
            
            NSDictionary *dict = array[j];
            
            if ([[dict valueForKey:@"code"] isEqualToString:self.countryCode]) {
                
                selectedIndex = [NSIndexPath indexPathForRow:j inSection:row];
                return;
            }
        }
        
        row ++;
    }
//
//    for (NSDictionary *dict  in dataRows) {
//        
//        nSelectedIndex ++;
//        if ([[dict valueForKey:@"code"] isEqualToString:self.countryCode]) {
//            
//            return;
//        }
//    }
}

- (void) initView {
    
    // customize UISearchBar
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setBackgroundColor:SelfColor(120, 120, 120)];
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor whiteColor]];
    [[UILabel appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor whiteColor]];
    
    if(selectedIndex != nil) {
        
        double delay = 0.1 * NSEC_PER_SEC;
        dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, llrint(delay));
        dispatch_after(time, dispatch_get_main_queue(), ^{
            [self.tableView scrollToRowAtIndexPath:selectedIndex
                                  atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
            
            CountryCell * cell = (CountryCell *)[_tableView cellForRowAtIndexPath:selectedIndex];
            
            [cell setSelected:YES animated:NO];
        });
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// ----------------------------------------------------------------------------------------------
#pragma mark - UISearchBarDelegate
// ----------------------------------------------------------------------------------------------
- (void) searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
    _searchField.showsCancelButton = YES;
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void) searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    
    _searchField.showsCancelButton = NO;
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void) searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [self.view endEditing:YES];
    
    searchBar.text = @"";
    
    searchActivie = NO;
    [self.tableView reloadData];
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    [searchBar resignFirstResponder];
}

- (void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    [filteredCountriesList removeAllObjects];
    
    if([searchText length] != 0) {
        
        searchActivie = YES;
        [self searchTableList];
        
    } else {
        
        searchActivie = NO;
    }
    
    [self.tableView reloadData];
}

- (void)searchTableList {
    
    NSString *searchString = _searchField.text;
    
    for (NSArray *array in dataRows) {
        
        for( int j = 0; j < [array count]; j ++) {
            
            NSDictionary *dict = array[j];
            
            NSComparisonResult result = [[dict valueForKey:kCountryName] compare:searchString options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchString length])];
            
            if (result == NSOrderedSame) {
    
                [filteredCountriesList addObject:dict];
            }
        }
    }
    
//    for(NSDictionary * dict in _dataRows) {
//        
//        NSComparisonResult result = [[dict valueForKey:kCountryName] compare:searchString options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchString length])];
//        if (result == NSOrderedSame) {
//            
//            [filteredCountriesList addObject:dict];
//        }
//    }
}

- (IBAction) backAction:(id)sender {
    
    [self gotoBack];
}

- (void) gotoBack {
    
    [self.navigationController popViewControllerAnimated:YES];
    
    // selectedIndexPath
    NSIndexPath *selectedIndexPath = [_tableView indexPathForSelectedRow];
    
    if(selectedIndexPath) {
        
        NSDictionary *selectedCountry;
        
        if(searchActivie) {
            
            selectedCountry = filteredCountriesList[selectedIndexPath.row];
        } else {
            
            NSArray *sectionArray = dataRows[selectedIndexPath.section];
            
            selectedCountry = sectionArray[selectedIndexPath.row];
        }
        
        [_delegate didSelectCountry:selectedCountry];
    }
}

#pragma mark - UITableView Datasource

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    
    if(searchActivie) {
        
        return 1;
    } else {
    
        return [sectionList count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (searchActivie) {
        return 0;
    }else{
        return 22.0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (searchActivie) {
        
        return [filteredCountriesList count];
        
    } else {
    
        return [dataRows[section] count];
    }
}

// return header
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    //viewforHeader
    id label = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"headerView"];
    
    if (!label) {
        
        label = [[UILabel alloc] init];
        [label setFont:[UIFont systemFontOfSize:14.5f]];
        [label setTextColor:[UIColor grayColor]];
        [label setBackgroundColor:[UIColor colorWithRed:240.0/255 green:240.0/255 blue:240.0/255 alpha:1]];
    }
    
    [label setText:[NSString stringWithFormat:@"  %@", sectionList[section]]];
    
    return label;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    
    if (searchActivie) {
        
        return nil;
    } else {
        
        return sectionList;
    }
}

- (NSInteger) tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    
    return index-1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"CountryCell";
    
    CountryCell *cell = (CountryCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil) {
        cell = [[CountryCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    
    if (searchActivie) {
        
        cell.textLabel.text = [[filteredCountriesList objectAtIndex:indexPath.row] valueForKey:kCountryName];
        cell.detailTextLabel.text = [[filteredCountriesList objectAtIndex:indexPath.row] valueForKey:kCountryCallingCode];
    } else {
        
        cell.textLabel.text = [[dataRows[indexPath.section] objectAtIndex:indexPath.row] valueForKey:kCountryName];
        cell.detailTextLabel.text = [[dataRows[indexPath.section] objectAtIndex:indexPath.row] valueForKey:kCountryCallingCode];
    }
    
    return cell;
}

#pragma mark - UITableView Delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self gotoBack];
}

@end


