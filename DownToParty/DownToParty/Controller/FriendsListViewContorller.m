//
//  FriendsListViewContorller.m
//  DownToParty
//
//  Created by victory on 4/9/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "FriendsListViewContorller.h"
#import "FriendListCell.h"
#import "FriendEntity.h"
#import "CommonUtils.h"

@interface FriendsListViewContorller () <UITableViewDelegate, UITableViewDataSource> {
    
    NSMutableArray *friendsList;
}

@property (nonatomic, weak) IBOutlet UITableView *tblFriendsList;

@end

@implementation FriendsListViewContorller

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    [self initVars];
    
    [self initView];
}

- (void) initVars {
   
    friendsList = [[NSMutableArray alloc] init];
    
    // real
//    friendsList = _user._friendsList;
}

- (void) initView {
    
    // navigation title
    int nFriendCount = 5;
    NSString *navTitleFriend = @"Friends ";
    NSString *navTitleFriendCount = [NSString stringWithFormat:@"%i", nFriendCount];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@", navTitleFriend, navTitleFriendCount]];
    NSDictionary *whiteBoldArial = @{NSForegroundColorAttributeName :[UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"Arial-BoldMT" size:20.0]};
    NSDictionary *yellowBoldHelvetica = @{NSForegroundColorAttributeName :[UIColor colorWithRed:255/255.0 green:242/255.0 blue:0/255.0 alpha:1.0], NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:20.0]};
    
    [attributedString addAttributes:whiteBoldArial range:NSMakeRange(0, navTitleFriend.length)];
    [attributedString addAttributes:yellowBoldHelvetica range:NSMakeRange(navTitleFriend.length+1, navTitleFriendCount.length)];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 21)];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    [lblTitle setAttributedText:attributedString];
    
    self.navigationItem.titleView = lblTitle;
    
    // remove cell seperater for empty cells
    self.tblFriendsList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma makr -
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 5;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"FriendListCell";
    
    FriendListCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    FriendEntity *_friend = [[FriendEntity alloc] init];
    
    if(indexPath.row >= 2) {
        
        _friend._state = DOWN_TO_CHILL;
    } else {
        
        _friend._state = DOWN_TO_PARTY;
    }
    
    switch (indexPath.row) {
        case 0:
            
            _friend._name = @"pearlgill";
            break;
            
        case 1:
            
            _friend._name = @"misshollytaylor";
            break;
            
        case 2:
            
            _friend._name = @"butcherboytom";
            break;
            
        case 3:
            
            _friend._name = @"santiagom27";
            break;
            
        case 4:
            
            _friend._name = @"mister_keating";
            break;
        default:
            break;
    }
    
    [cell setFriend:_friend];
    
    if(indexPath.row % 2 == 0) {
        
        cell.imvProfile.image = [UIImage imageNamed:@"test1.png"];
    } else {
        
        cell.imvProfile.image = [UIImage imageNamed:@"test2.png"];
    }
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 68.0;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self performSegueWithIdentifier:@"SegueFriendList2Chat" sender:self];
}

- (IBAction) addFriendAction:(id)sender {
    
    NSLog(@"add friend!!!");
}

- (IBAction) backwardAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction) forwardAction:(id)sender {
    
    [self performSegueWithIdentifier:@"SegueFriends2ChatList" sender:self];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
