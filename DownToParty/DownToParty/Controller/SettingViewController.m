//
//  SettingViewController.m
//  DownToParty
//
//  Created by victory on 4/9/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "SettingViewController.h"
#import "StatusViewController.h"
#import "CommonUtils.h"
#import "UserEntity.h"

@interface SettingViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate> {

    UserEntity *_user;
}

@property (nonatomic, weak) IBOutlet UIImageView *imvProfile;
@property (nonatomic, weak) IBOutlet UILabel *lblUserName;

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initVars];
    
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initVars {
    
    _user = APPDELEGATE.Me;
}

- (void) initView {
    
    self.imvProfile.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _lblUserName.text = _user._name;
    
    [_imvProfile setImageWithURL:[NSURL URLWithString:_user._photoUrl] placeholderImage:[UIImage imageNamed:@"test1"]];
}

- (IBAction) selectPhoto:(id)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self openCamera];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self openGallery];
    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    actionSheet.view.tintColor = [UIColor lightGrayColor];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void) deletePhoto {
    
}

- (void) openCamera {
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    } else {
        
        NSLog(@"No Cameran\n. Please test on device");
    }
}

- (void) openGallery {
    
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController: imagePicker animated:YES completion:nil];
}

#pragma mark -
#pragma mark - UIImagePickerControllerDelegate

// This method is called when an image has been chosen from the album or taken from the camera
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage * chosenImage = info[UIImagePickerControllerEditedImage];
    
    self.imvProfile.image = chosenImage;
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
    [picker dismissViewControllerAnimated:YES completion:^{
        
//        dispatch_queue_t writeQueue = dispatch_queue_create("SavePhoto", NULL);
//        
//        dispatch_async(writeQueue, ^{
//            
//            NSString * strPhotoPath = [CommonUtils saveToFile:chosenImage isProfile:YES];
//            
//            dispatch_async(dispatch_get_main_queue(), ^ {
//                
//                photoPath = strPhotoPath;
//                
//                // update ui (set profile image with saved Photo URL
//                [imvAavatar setImage:[UIImage imageWithContentsOfFile:strPhotoPath]];
//            });
//        });
    }];
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction) forwardAction:(id)sender {
    
    // get the existing navigationController view stack - current zero
    NSMutableArray *newViewContorllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    
    [newViewContorllers removeObjectAtIndex:0];
    
    // push StatusViewController
    StatusViewController *statusVC = (StatusViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"StatusViewController"];
    [newViewContorllers addObject:statusVC];
    
    // set new view controllers in the navigation controller stack
    [self.navigationController setViewControllers:newViewContorllers animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
