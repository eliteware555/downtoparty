//
//  SplashViewController.m
//  DownToParty
//

#import "SplashViewController.h"
#import "AddPhoneNumberTableViewController.h"
#import "StatusViewController.h"
#import "CommonUtils.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    BOOL isLoggedIn = [CommonUtils getUserLogin];
    
    if(isLoggedIn) {
        
        [self performSelector:@selector(gotoHome) withObject:[NSNumber numberWithBool:YES] afterDelay:1.5f];
        
    } else {
        
        [self performSelector:@selector(gotoLogin) withObject:[NSNumber numberWithBool:YES] afterDelay:1.5f];
    }
}

// go to StatusViewController
- (void) gotoHome {
    
    UINavigationController *homeNav = (UINavigationController *) [self.storyboard instantiateViewControllerWithIdentifier:@"MainNav"];
    
    [[UIApplication sharedApplication].keyWindow setRootViewController:homeNav];
}

// go to AddPhoneNumberViewController
- (void) gotoLogin {
    
    UINavigationController *signinNav = (UINavigationController *) [self.storyboard instantiateViewControllerWithIdentifier:@"SigninNav"];
    [[UIApplication sharedApplication].keyWindow setRootViewController:signinNav];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
