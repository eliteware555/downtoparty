//
//  StatusViewController.m
//  DownToParty
//
//  Created by victory on 4/9/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "StatusViewController.h"
#import "UITextView+Placeholder.h"
#import "CommonUtils.h"
#import "UIImage+ImageWithColor.h"
#import "SettingViewController.h"
#import "UserEntity.h"

@interface StatusViewController () {
    
    UserEntity *_user;
}

@property (nonatomic, weak) IBOutlet UIButton *btnStateDownToParty;
@property (nonatomic, weak) IBOutlet UIButton *btnStateDownToChill;
@property (nonatomic, weak) IBOutlet UITextView *txvCreatedState;

@property (nonatomic, weak) IBOutlet UISegmentedControl *segPush;
@property (nonatomic, weak) IBOutlet UISegmentedControl *segLocation;

@property (nonatomic, weak) IBOutlet UIButton *btnSetStatus;

@end

@implementation StatusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initVar];
    
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) initVar {
    
    _user = APPDELEGATE.Me;
    
}

- (void) initView {
    
    // textview offset, placeholder
    [self.txvCreatedState setContentInset:UIEdgeInsetsMake(0, 20, 0,-20)];
    self.txvCreatedState.placeholder = @"Create your own";
    
    // set border color
    self.btnStateDownToParty.layer.borderColor = SelfColor(68, 68, 68).CGColor;
    self.btnStateDownToChill.layer.borderColor = SelfColor(255, 255, 255).CGColor;
    
    // customize UISegmentedControl
    [self.segPush setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]} forState:UIControlStateSelected];
    [self.segPush setTitleTextAttributes:@{NSForegroundColorAttributeName: SelfColor(123, 115, 97)} forState:UIControlStateNormal];
    
    //
    [self.btnSetStatus setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithRed:123/255.0 green:115/255.0 blue:97/255.0 alpha:1.0]] forState:UIControlStateNormal];
    [self.btnSetStatus setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithRed:123/255.0 green:115/255.0 blue:97/255.0 alpha:0.7]] forState:UIControlStateFocused];
    
    [_segPush setSelectedSegmentIndex:_user._isAllowedPush ? 0 : 1];
    
    [_segLocation setSelectedSegmentIndex:_user._isAllowedLocService ? 0 : 1];
}

- (IBAction) selectDownToParty:(id)sender {
    
    [self selectState:DOWN_TO_PARTY];
}

- (IBAction) selectDownToChill:(id)sender {
    
    [self selectState:DOWN_TO_CHILL];
}

- (void) selectState:(int)state {
    
    if(state == DOWN_TO_PARTY) {
        
        // set border color
        self.btnStateDownToParty.layer.borderColor = SelfColor(68, 68, 68).CGColor;
        self.btnStateDownToChill.layer.borderColor = SelfColor(255, 255, 255).CGColor;
    } else {
        
        // set border color
        self.btnStateDownToParty.layer.borderColor = SelfColor(255, 255, 255).CGColor;
        self.btnStateDownToChill.layer.borderColor = SelfColor(68, 68, 68).CGColor;
    }
}

- (IBAction) pushSettingChanged:(UISegmentedControl *)sender {
    
    if(sender.selectedSegmentIndex == 0) {
        
        NSLog(@"enable sending push notification");
        
    } else {
        
        NSLog(@"disable sending push notification.");
    }
}

- (IBAction) addLocationSettingChanged:(UISegmentedControl *)sender {
    
    if(sender.selectedSegmentIndex == 0) {
        
        NSLog(@"enable adding party location");
        
    } else {
        
        NSLog(@"disable adding party location");
    }
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}

- (IBAction) backwardAction:(id)sender {
    
    // get the existing navigationController view stack - current zero
    NSMutableArray *newViewContorllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];

    // push SettingViewController
    SettingViewController *settingVC = (SettingViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"SettingViewController"];
    [newViewContorllers insertObject:settingVC atIndex:0];

    // set new view controllers in the navigation controller stack
    [self.navigationController setViewControllers:newViewContorllers animated:YES];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction) forwardAction:(id)sender {
    
    [self performSegueWithIdentifier:@"SegueStatus2Friends" sender:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
