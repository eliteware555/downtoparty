//
//  VerifyTableViewController.m
//  DownToParty
//
//  Created by victory on 4/9/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "VerifyTableViewController.h"
#import "CommonUtils.h"
#import "ReqConst.h"
#import "UserEntity.h"
#import "NSString+Encode.h"
#import "ViewUtils.h"
#import "OCMaskedTextFieldView.h"

#define LIGHT_FONT  [UIFont fontWithName:@"Avenir-Light"  size:14]
#define MEDIUM_FONT [UIFont fontWithName:@"Avenir-Medium" size:25]

#define DARK_COLOR             [UIColor colorWithRed: 68/255.0 green: 68/255.0 blue: 68/255.0 alpha:1.0]
#define KILL_LA_KILL_RED_COLOR [UIColor colorWithRed:221/255.0 green: 45/255.0 blue: 31/255.0 alpha:1.0]

@interface VerifyTableViewController () {

    UserEntity *_user;
    
    NSString *authCode;
    
    OCMaskedTextFieldView *txtVerifyCode;
    
    UIActivityIndicatorView *activityIndicator;
}

@property (nonatomic, weak) IBOutlet UIView *vSuperVerifyCode;

@end

@implementation VerifyTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self initVars];
    
    [self initView];
}

- (void) initVars {
    
    _user = APPDELEGATE.Me;
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [activityIndicator setColor:[UIColor darkGrayColor]];
    activityIndicator.hidesWhenStopped = YES;
}

- (void) initView {
    
    // set navigation title with phone numer
    NSString *navTitle = [NSString stringWithFormat:@"%@ %@", _user._dialCode, _user._phoneNumber];
    self.navigationItem.title = navTitle;
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    // set textField style
    
    txtVerifyCode = [[OCMaskedTextFieldView alloc]
                                         initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width - 20, self.vSuperVerifyCode.bounds.size.height)
                                         andMask:@"#  #  #  #  #  #  #  #"
                                         showMask:YES];
    [[txtVerifyCode maskedTextField] setBorderStyle:UITextBorderStyleNone];
    [[txtVerifyCode maskedTextField] setFont:MEDIUM_FONT];
    [[txtVerifyCode maskedTextField] setTintColor:[UIColor clearColor]];
    [[txtVerifyCode maskedTextField] setTextAlignment:NSTextAlignmentCenter];
    [[txtVerifyCode maskedTextField] setKeyboardAppearance:UIKeyboardAppearanceLight];
    [self setupTextField:[txtVerifyCode maskedTextField]];
    [self.vSuperVerifyCode addSubview:txtVerifyCode];
    
    [[txtVerifyCode maskedTextField] becomeFirstResponder];
    
    [[txtVerifyCode maskedTextField] addTarget:self action:@selector(textFieldDidChanged:) forControlEvents:UIControlEventValueChanged];
}

- (void)setupTextField:(UITextField*)textField
{
    textField.backgroundColor    = [UIColor whiteColor];
    textField.layer.borderColor  = [UIColor whiteColor].CGColor;
    textField.layer.borderWidth  = 0.0;
    textField.layer.cornerRadius = 1.0;
    if (textField.textAlignment != NSTextAlignmentCenter)
    {
        textField.layer.sublayerTransform = CATransform3DMakeTranslation(8, 0, 0);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextField Delegate
- (void) textFieldDidChanged: (UITextField *) textField {
    
    NSLog(@"%@", textField.text);
    
    if (textField.text.length >= 14) {
        
        [self confirmAuthCode];
    }
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 1;
}

- (void) showLoadingView:(NSString *) title {
    
    UIView *maskView = [[UIView alloc] initWithFrame:CGRectMake(0, -20, [[UIScreen mainScreen] applicationFrame].size.width, [[UIScreen mainScreen] applicationFrame].size.height + 20.0f)];
    [maskView setBackgroundColor:[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:0.90]];
    maskView.userInteractionEnabled = YES;
    maskView.tag = 100;
    
    [self.navigationController.navigationBar addSubview:maskView];
    [self.navigationController.navigationBar bringSubviewToFront:maskView];
    
    [maskView addSubview:activityIndicator];
    activityIndicator.center = maskView.contentCenter;
    
    UILabel *lblConnecting = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, maskView.bounds.size.width, 30.0f)];
    lblConnecting.textColor = [UIColor darkGrayColor];
    lblConnecting.font = [UIFont systemFontOfSize:20.0f];
    lblConnecting.text = title;
    lblConnecting.textAlignment = NSTextAlignmentCenter;
    [maskView addSubview:lblConnecting];
    
    lblConnecting.bottom = maskView.bounds.size.height / 2.0 + 50.0f;
    lblConnecting.tag = 101;
    
    [activityIndicator startAnimating];
}

- (void) hideLoadingView {
    
    [activityIndicator stopAnimating];
    [[self.navigationController.navigationBar viewWithTag:100] removeFromSuperview];
}

- (void) confirmAuthCode {
    
    [self.view endEditing:YES];
    
    [self showLoadingView:[NSString stringWithFormat:@"%@", VERIFYING]];
    
    NSString *strAuthCode = [[txtVerifyCode.maskedTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]stringByReplacingOccurrencesOfString:@"  " withString:@""];
    strAuthCode = [strAuthCode encodeString:NSUTF8StringEncoding];
    
    // make server url
    NSString * url = [NSString stringWithFormat:@"%@%@/%d/%@", SERVER_URL, REQ_AUTHCONFIRM, _user._idx, strAuthCode];
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        [self hideLoadingView];
        
        int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResult_Code == CODE_SUCCESS) {
            
            NSDictionary *dict = [responseObject objectForKey:RES_USERINFO];
            
            _user._name = [dict valueForKey:RES_NAME];
            _user._photoUrl = [dict valueForKey:RES_PHOTOURL];
            
            [self gotoProfile];
            
        } else {
            
            [[JLToast makeText:WRONG_AUTH duration: 3] show];
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        [self hideLoadingView];
        
        [APPDELEGATE showAlertDialog:nil message:CONN_ERROR positive:ALERT_OK negative:nil sender:self];
        
        NSLog(@"Error: %@", error);
    }];
}

- (IBAction) confirmAuthCodeAction:(id)sender {
    
    // for a test
    [self gotoProfile];
    
//    authCode = [[txtVerifyCode.maskedTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] stringByReplacingOccurrencesOfString:@"  " withString:@""];
//    authCode = [authCode stringByReplacingOccurrencesOfString:@"_" withString:@""];
//    
//    NSLog(@"%@", authCode);
//    
//    if(authCode.length < 8) {
//        
//        [APPDELEGATE showAlertDialog:nil message:INPUT_CORRECT_AUTHCODE positive:ALERT_OK negative:nil sender:self];
//        return;
//    }
//    
//    [self confirmAuthCode];
}

- (IBAction) backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) gotoProfile {
    
    [self performSegueWithIdentifier:@"Segue2YourAccount" sender:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
