//
//  FriendEntity.h
//  DownToParty
//
//  Created by victory on 4/9/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FriendEntity : NSObject

@property (nonatomic) int _idx;                         // user index
@property (nonatomic, strong) NSString *_name;          // user name
@property (nonatomic, strong) NSString *_photoURL;      // user profile image url
@property (nonatomic) int _state;                        // user's current state 0 - Down to party, 1 - Down to chill

@property (nonatomic, strong) NSString *_phoneNumber;   // user's phone number

@end
