//
//  RoomEntity.h
//  DownToParty
//
//  Created by Victory on 4/10/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RoomEntity : NSObject

@property (nonatomic, retain) NSString *_name;                  // initial created room name (1_2_3) - user_id
@property (nonatomic, retain) NSString * _participantsName;     // participants : (1_2_3_4_5)        - participants
@property (nonatomic, retain) NSString  *_displayName;          // room displayname (with user's name)

@property (nonatomic, retain) NSString * _recentContent;        // last content
@property (nonatomic, retain) NSString * _recentDate;           // last received date & time

@property (nonatomic) int _recentCounter;                       // unread message count
@property (nonatomic) int _currentUsers;                        // current room participant numbers

@property (nonatomic, retain) NSMutableArray *_participants;    // room participant's array
@property (nonatomic, retain) NSMutableArray *_chatList;        // chat entity array

@property (nonatomic) BOOL _isSelected;                         // selected or not

@end
