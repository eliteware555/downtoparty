//
//  RoomEntity.m
//  DownToParty
//
//  Created by Victory on 4/10/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "RoomEntity.h"

@implementation RoomEntity

@synthesize _name, _displayName, _participantsName, _recentContent, _recentDate;

@synthesize _recentCounter, _currentUsers;

@synthesize _participants, _chatList;

@synthesize _isSelected;

- (instancetype) init {
    
    if (self = [super init]) {
        
        // initialize code here
        _name = @"";
        _participantsName = @"";
        _displayName = @"";
        
        _recentContent = @"";
        _recentDate = @"";
        _recentCounter = 0;
        
        _currentUsers = 0;
        
        _isSelected = NO;
        
        _participants = [[NSMutableArray alloc] init];
        _chatList = [[NSMutableArray alloc] init];
    }
    
    return self;
}

@end
