//
//  UserEntity.h
//  DownToParty
//
//  Created by Victory on 4/11/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserEntity : NSObject

@property (nonatomic) int _idx;                                 // user identifier (unique value)

@property (nonatomic, retain) NSString * _name;                 // user name
@property (nonatomic, retain) NSString * _photoUrl;             // user profile image path

@property (nonatomic) BOOL _isAllowedPush;                      // push setting state - 1:push, 0: not push
@property (nonatomic) BOOL _isAllowedLocService;                // allow location service - 1

@property (nonatomic, strong) NSString *_countryCode;            // countryCode: CN
@property (nonatomic, strong) NSString *_countryName;            // country name : China
@property (nonatomic, strong) NSString *_dialCode;               // dial code : +86
@property (nonatomic, retain) NSString * _phoneNumber;           // user own phone number

@property (nonatomic, strong) NSMutableArray * _frList;         // friendList
@property (nonatomic, strong) NSMutableArray * _roomList;       // chatting room list

@end
