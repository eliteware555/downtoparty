//
//  UserEntity.m
//  DownToParty
//
//  Created by Victory on 4/11/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "UserEntity.h"

@implementation UserEntity

@synthesize _idx;
@synthesize _name, _photoUrl, _countryCode, _dialCode, _countryName, _phoneNumber;
@synthesize _isAllowedPush;
@synthesize _frList, _roomList;

- (instancetype) init {
    
    if(self = [super init]) {
        
        // initialize code here
        _idx = -1;          // invalid user
        
        _name = @"";
        _photoUrl = @"";
        _countryCode = @"";
        _dialCode = @"";
        _countryName = @"";
        _phoneNumber = @"";
        
        _isAllowedPush = YES;
        
        _frList = [[NSMutableArray alloc] init];
        _roomList = [[NSMutableArray alloc] init];
        
        // get country name, country code
        [self initVars];
    }
    
    return self;
}

// get country code & country name
- (void) initVars {
    
    NSString *countryCode = [[NSLocale currentLocale] objectForKey:NSLocaleCountryCode];
    NSString *countryName = [[NSLocale currentLocale] displayNameForKey:NSLocaleCountryCode value:countryCode];
    
    _countryCode = countryCode;
    _countryName = countryName;
    
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"countries" ofType:@"json"]];
    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    
    if (localError != nil) {
        NSLog(@"%@", [localError userInfo]);
    }
    
    NSArray *countriesList;
    countriesList = (NSArray *)parsedObject;
    
    for(NSDictionary *dict in countriesList) {
        
        if([[dict valueForKey:@"code"] isEqualToString:countryCode]) {
            
            _dialCode = [dict valueForKey:@"dial_code"];
            return;
        }
    }
}

@end
