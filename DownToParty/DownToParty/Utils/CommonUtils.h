//
//  ImageUtil.h
//  DChat
//
//  Created by ChenJunLi on 12/16/15.
//  Copyright © 2015 Elite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Constants.h"
#import "AppDelegate.h"
#import "UserDefault.h"
#import <AudioToolbox/AudioToolbox.h>

@interface CommonUtils : NSObject


+ (void) saveUserInfo;
+ (void) loadUserInfo;


+ (void) setUserLogin:(BOOL) isLoggedIn;
+ (BOOL) getUserLogin;


+ (void) saveDeviceToken:(NSString *) deviceToken;
+ (NSString *) getDeviceToken;


+ (void) vibrate;

+ (NSString *) saveToFile:(UIImage *) srcImage isProfile:(BOOL) isProfile;

@end





