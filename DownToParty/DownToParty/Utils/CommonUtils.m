//
//  ImageUtil.m
//  DChat
//
//  Created by ChenJunLi on 12/16/15.
//  Copyright © 2015 Elite. All rights reserved.
//

#import "CommonUtils.h"
#import "UIImage+ResizeMagick.h"
#import <AudioToolbox/AudioToolbox.h>

@implementation CommonUtils

-(id) init {
    
    self = [super init];
    if(self) {
        // custom init code
    }
    
    return self;
}

//func isValidEmail(email:String) -> Bool {
//    let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
//    
//    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
//    return emailTest.evaluateWithObject(email)
//}


//+ (BOOL) isValidEmail: (NSString *) email {
//    
//    BOOL stricterFilter = NO; 
//    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
//    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
//    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
//    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
//    return [emailTest evaluateWithObject:email];
//}
//
// resize image with specified size
+ (UIImage *) imageResize: (UIImage *) srcImage {
    
    return [srcImage resizedImageByMagick:@"128x128#"];
}

+ (UIImage *) imageResizeforChat: (UIImage *) scrImage {
    
    return [scrImage resizedImageByMagick:@"256x256#"];
}


// save image to file (Documents/Campus/profile.png
+ (NSString *) saveToFile:(UIImage *) srcImage isProfile:(BOOL) isProfile {
    
    NSString * savedPhotoURL = nil;
    
    NSString * outputFileName = @"";
    UIImage * outputImage;
    
    NSLog(@"scr width = %f, scr height = %f", srcImage.size.width, srcImage.size.height);
    
    // set output file name and resize source image with output image size
    if(isProfile) {
        
        outputFileName = @"profile.jpg";
        outputImage = [CommonUtils imageResize:srcImage];
        
    } else {
        
        outputFileName = @"chatfile.jpg";
        outputImage = [CommonUtils imageResizeforChat:srcImage];
    }
    
    NSLog(@"out width = %f, out height = %f", outputImage.size.width, outputImage.size.height);
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentsDirectory = [paths objectAtIndex:0];
    
    // current document directory
    [fileManager changeCurrentDirectoryPath:documentsDirectory];
    [fileManager createDirectoryAtPath:SAVE_ROOT_PATH withIntermediateDirectories:YES attributes:nil error:NULL];
    
    // Documents/Campus
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:SAVE_ROOT_PATH];
    NSString * filePath = [documentsDirectory stringByAppendingPathComponent:outputFileName];
    
    NSLog(@"save filePath = %@", filePath);
    
    // if the file exists already, delete and write, else if create filePath
    if([fileManager fileExistsAtPath:filePath]) {
        [fileManager removeItemAtPath:filePath error:nil];
    } else {
        [fileManager createFileAtPath:filePath contents:nil attributes:nil];
    }
    
    // Write a UIImage to PNG file
    //    [UIImagePNGRepresentation(outputImage) writeToFile:filePath atomically:YES];
//    if(isProfile) {
        [UIImageJPEGRepresentation(outputImage, 1.0) writeToFile:filePath atomically:YES];
//    } else {
//        [UIImageJPEGRepresentation(outputImage, 0.8) writeToFile:filePath atomically:YES];
//    }
    
    NSError * error;
    [fileManager contentsOfDirectoryAtPath:documentsDirectory error:&error];
    
    return savedPhotoURL = filePath;
}

+ (void) saveUserInfo {
    
    [CommonUtils setUserIdx:APPDELEGATE.Me._idx];
    [CommonUtils setUserPhotoUrl:APPDELEGATE.Me._photoUrl];
    [CommonUtils setPushSetting:APPDELEGATE.Me._isAllowedPush];
    [CommonUtils setLocServiceSetting:APPDELEGATE.Me._isAllowedLocService];
}

+ (void) loadUserInfo {
    
    [CommonUtils setDefaultLocService];
    [CommonUtils setDefaultPushNotification];
    
    if(APPDELEGATE.Me) {
        
        APPDELEGATE.Me._idx = [CommonUtils getUserIdx];
        APPDELEGATE.Me._name = [CommonUtils getUserName];
        APPDELEGATE.Me._photoUrl = [CommonUtils getUserPhotoUrl];
        
        APPDELEGATE.Me._isAllowedPush = [CommonUtils getPushSetting];
        APPDELEGATE.Me._isAllowedLocService = [CommonUtils getLocServiceSetting];
    }
}

+ (void) setPushSetting: (BOOL) value {
    
    [UserDefault setBoolValue:PREFKEY_USER_ALLOWPUSH value:value];
}

+ (BOOL) getPushSetting {
    
    return [UserDefault getBoolValue:PREFKEY_USER_ALLOWPUSH];
}

+ (void) setLocServiceSetting: (BOOL) value {
    
    [UserDefault setBoolValue:PREFKEY_USER_LOCSERVICE value:value];
}

+ (BOOL) getLocServiceSetting {
    
    return [UserDefault getBoolValue:PREFKEY_USER_LOCSERVICE];
}

+ (void) setDefaultPushNotification {
 
    NSDictionary *userDefaultsDefaults = [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithBool:YES], PREFKEY_USER_ALLOWPUSH, nil];
    [USERDEFAULTS registerDefaults:userDefaultsDefaults];
}

+ (void) setDefaultLocService {
    
    NSDictionary *userDefaultsDefaults = [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithBool:YES], PREFKEY_USER_LOCSERVICE, nil];
    [USERDEFAULTS registerDefaults:userDefaultsDefaults];
}

+ (void) setUserIdx:(int) idx {
    
    [UserDefault setIntValue:PREFKEY_ID value:idx];
}

+ (int) getUserIdx {
    
    return [UserDefault getIntValue:PREFKEY_ID];
}

+ (void) setUserName: (NSString *) name {
    
    [UserDefault setStringValue:PREFKEY_FULLNAME value:name];
}

+ (NSString *) getUserName {
    
    NSString *res = [UserDefault getStringValue:PREFKEY_FULLNAME];
    return res == nil ? @"" : res;
}


+ (void) setUserPhotoUrl:(NSString *) photoPath {
    
    [UserDefault setStringValue:PREFKEY_PHOTOURL value:photoPath];
}

+ (NSString *) getUserPhotoUrl {
    
    NSString *res = [UserDefault getStringValue:PREFKEY_PHOTOURL];
    return res == nil ? @"" : res;
}

+ (void) setUserLogin:(BOOL) isLoggedIn {
    
    [UserDefault setBoolValue:PREFKEY_LOGGED_IN value:isLoggedIn];
}

+ (BOOL) getUserLogin {
    
    return [UserDefault getBoolValue:PREFKEY_LOGGED_IN];
}

+ (void) saveDeviceToken:(NSString *) deviceToken {
    
    [UserDefault setStringValue:PREFKEY_DEVICE_TOKEN value:deviceToken];
}

+ (NSString *) getDeviceToken {
    
    return [UserDefault getStringValue:PREFKEY_DEVICE_TOKEN];
}

+ (void) vibrate {
    
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}


@end







