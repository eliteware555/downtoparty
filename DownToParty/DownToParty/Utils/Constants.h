//
//  Constants.h
//  DChat
//
//  Created by ChenJunLi on 12/12/15.
//  Copyright © 2015 Elite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <JLToast/JLToast-Swift.h>
@import AFNetworking;
@import JLToast;

@interface Constants : NSObject

/**
 **     Constants define Macro
 **/
#pragma mark -
#pragma mark - constans define

#define DOWN_TO_PARTY                           0
#define DOWN_TO_CHILL                           1

#define PROFILE_IMG_SIZE                        120

#define SAVE_ROOT_PATH                          @"DownToParty"

/**
 **     Userdefaults and AppDelegate Macro
 **/
#pragma mark -
#pragma mark - UserDefaults and AppDelegate

#define USERDEFAULTS [NSUserDefaults standardUserDefaults]
#define APPDELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])

/**
 **  Constans Define Macro
 **/

#pragma mark -
#pragma mark - Constants Macro

// -------------------------------------------------------------
// string define
// -------------------------------------------------------------

// alert common
#define     ALERT_TITLE                     @"DownToParty"
#define     ALERT_YES                       @"Yes"
#define     ALERT_EDIT                      @"Edit"
#define     ALERT_OK                        @"OK"
#define     ALERT_CANCEL                    @"CANCEL"
#define     ALERT_CONFIRM_PHONENUMBER       @"NUMBER CONFIRMATION:"
#define     ALERT_MESSAGE_PHONENUMBER       @"Is your phonenumber above correct?"




// server connect & loading
#define     LOADING                         @"connecting..."
#define     VERIFYING                       @"verifying "

#define     CONN_ERROR                      @"Connecting to the server failed.\nPlease try again."

#define     WRONG_PHONENUMBER               @"Phonenumber is wrong."
#define     WRONG_AUTH                        @"Please input verification code correctly."



// register phone number
#define     INPUT_DIALCODE                  @"Input dial code, please."
#define     INPUT_PHONENUMBER               @"Input your phonenumber, pelase."
#define     INPUT_CORRECT_AUTHCODE          @"Input your verification code correctly."

// signup
#define INPUT_FULL_NAME             @"Input your name, please"
#define INPUT_BIRTHDAY              @"Please input your birthday."
#define INPUT_EMAIL                 @"Please enter your email address."
#define INPUT_CORRECT_EMAIL         @"Please enter your email address correctly."

#define INPUT_CONFIRM_EMAIL         @"Please re-enter your email address."
#define INPUT_CORRECT_CONFIRM_EMAIL         @"Please re-enter your email address correctly."

#define CHECK_EMAIL_INPUT           @"Please check your input."

#define SELECT_PHOTO                @"Please add your photo."
#define INPUT_PWD                   @"Please enter a password."
#define INPUT_CONFIRM_PWD           @"Please enter your password again."
#define INPUT_CORRECT_PWD           @"Please enter the password correctly"

#define CHECK_MAILBOX               @"Please check your mailbox. \n You've got your password."
#define CHECK_VERIFICODE            @"Please check your mainbox. \n You'ver got verification code."

// signup about me
#define INPUT_TAG_LINE              @"Please input tagline."
#define INPUT_ABOUT_ME              @"Write about you, please."
#define INPUT_INTEREST              @"Please input interested in."
#define INPUT_GENDER                @"Please select sex."
#define INPUT_SEXUALLITY            @"Please select sexuallity."

#define INPUT_VERIFYCODE            @"Please input verification code."



#define EMAIL_EXIST                 @"This email already exists."
#define REGISTER_FAIL               @"Registration failed.\nTry again later."

#define PHOTO_UPLOAD_FAIL           @"Picture registration failed."
#define UNREGISTERED_USER           @"Unregistered user."
#define WRONG_PASSWROD              @"Pasword is wrong."
#define WRONG_EMAILADDRESS          @"Email address is wrong."

#define WRITE_COMMENT               @"Please write comment."

#define PHOTO_UPLOAD_SUCCESS        @"The uploaded file was successful."
#define FAIL_UPLOAD_FILE            @"Fail to upload a file"


#define FAIL_FRIEND                 @"Sorry, Try again later."

#define SUCCESS_FREINDREQUEST       @"Just sent you a friend request."
#define SUCCESS_SETFRIEND           @"Successfully it has been added."
#define SUCESSS_BLOCKFRIEND         @"Successfully blocked."

#define SELECT_AGE                  @"It's available more than 16 years old"

#define   NAV_TITLE                 @"The University Of Texas - Arlington"

#define   INPUT_FIRSTNAME           @"Please input your first name."
#define   INPUT_LASTNAME            @"Please input your last name."


#define   INPUT_CODE                @"Please input verification code."

#define   NOCLASS                   @"There is no class. Please add your class."

#define   SELECT_MOVIE              @"Write your favorite movie."
#define   SELECT_ABOUTME            @"Write about you."

#define   CHATTING_ERROR            @"The connection to the chat server has been shut down."
#define   CHATTING_SUCCCESS         @"It was connected to the chat server."
#define   CHATTING_CONNECTING       @"It is being connected to the chat server."

#define   INVALIDEMAIL              @"Please enter a valid email address."
#define   NOT_AUTHENTICATE          @"E-mail has not been certified."




#define   CHECK_PWD                 @"Please enter the password correctly."
#define   UPDATE_SUCCESS            @"Succeed to update profile."
#define   ADDFRIEND_SUCCESS         @"Friend request sent."
#define   UNFRIEND_SUCCESS          @"Succeed to unfriend."
#define   ADDFRIEND_FAIL            @"Failed to add friend."
#define   UNFRIEND_FAIL             @"Failed to unfriend."
#define   ADDCLASS_SUCCESS          @"Succeed to add class."
#define   CONFIRM_DELETECLASS       @"Do you want to delete this class?"
#define   RESETPWD_SUCCESS          @"Succeed to reset password."
#define   CHECK_CODE                @"Please check your mail, and input the verification code."

#define TIME_AM                                 @"AM"
#define TIME_PM                                 @"PM"

#define KEY_TIME_SEPERTATOR                     @"#"
#define KEY_FILE_MARKER                         @"FILE#"
#define KEY_IMAGE_MARKER                        @"IMAGE#"
#define KEY_VIDEO_MARKER                        @"VIDEO#"
#define KEY_ROOM_MARKER                         @"ROOM#"

#define SelfColor(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]


extern BOOL bEnteredBackground;                 // when the app is entering in a background, it wil be set as a true.
                                                // after the app is entering in a foreground, it will be set as a false.

extern BOOL bIsShownChatView;                   // if the current topviewcontroller is ChatViewController, true

extern BOOL bIsShownChatList;                   // the current topviewcontroller is ChatListViewController, true 
@end









