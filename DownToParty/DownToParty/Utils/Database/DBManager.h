//
//  DBManager.h
//  DChat
//
//  Created by ChenJunLi on 12/18/15.
//  Copyright © 2015 Elite. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DBManager : NSObject {

    NSString * databasePath;
}

@property (nonatomic, strong) NSString *documentsDirectory;
@property (nonatomic, strong) NSString *databaseFilename;

@property (nonatomic, strong) NSMutableArray *arrResults;

@property (nonatomic, strong) NSMutableArray *arrColumnNames;

@property (nonatomic) int affectedRows;

@property (nonatomic) long long lastInsertedRowID;

+ (DBManager *) getSharedInstance;

- (void) clearDB;

- (void) saveVideoURL:(int) _idx url:(NSString *) url;
- (NSURL *) getVideoURL:(int) _idx;

@end