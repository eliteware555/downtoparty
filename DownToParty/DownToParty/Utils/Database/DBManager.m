//
//  DBManager.m
//  DChat
//
//  Created by ChenJunLi on 12/18/15.
//  Copyright © 2015 Elite. All rights reserved.
//

#import "DBManager.h"

#define MAX_LOAD_CNT 20


static DBManager * sharedInstance = nil;
static sqlite3 *database = nil;
//static sqlite3_stmt * statement = nil;

@interface DBManager()

- (BOOL) createDB;

-(void)runQuery:(const char *)query isQueryExecutable:(BOOL)queryExecutable;

-(NSArray *)loadDataFromDB:(NSString *)query;

-(void)executeQuery:(NSString *)query;

@end

@implementation DBManager

+ (DBManager *) getSharedInstance {
    
    if(!sharedInstance) {
        sharedInstance = [[super allocWithZone:NULL] init];
        [sharedInstance createDB];
    }
    
    return sharedInstance;
}

- (BOOL) createDB {
    
    NSString *docsDir;
    NSArray *dirPaths;
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString:[docsDir stringByAppendingPathComponent: @"dchatDB.db"]];
    
    BOOL isSuccess = YES;
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            char *errMsg;
            const char *sql_stmt = "CREATE TABLE IF NOT EXISTS URLTABLE (ID INTEGER PRIMARY KEY AUTOINCREMENT, USERID INTEGER , VIDEOURL TEXT)";
            if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                isSuccess = NO;
                NSLog(@"Failed to create url table %s", errMsg);
            }
            
            sqlite3_close(database);
            NSLog(@"created database");
            return  isSuccess;
        }
        else {
            isSuccess = NO;
            NSLog(@"Failed to open/create database");
        }
    }
    
    return isSuccess;
}

-(void)runQuery:(const char *)query isQueryExecutable:(BOOL)queryExecutable{
    // Create a sqlite object.
    sqlite3 *sqlite3Database;
    
//    // Set the database file path.
//    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    // Initialize the results array.
    if (self.arrResults != nil) {
        [self.arrResults removeAllObjects];
        self.arrResults = nil;
    }
    self.arrResults = [[NSMutableArray alloc] init];
    
    // Initialize the column names array.
    if (self.arrColumnNames != nil) {
        [self.arrColumnNames removeAllObjects];
        self.arrColumnNames = nil;
    }
    self.arrColumnNames = [[NSMutableArray alloc] init];
    
    // Open the database.
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    if(openDatabaseResult == SQLITE_OK) {
        // Declare a sqlite3_stmt object in which will be stored the query after having been compiled into a SQLite statement.
        sqlite3_stmt *compiledStatement;
        
        // Load all data from database to memory.
        if(sqlite3_prepare_v2(sqlite3Database, query, -1, &compiledStatement, NULL) == SQLITE_OK) {
            // Check if the query is non-executable.
            if (!queryExecutable){
                // In this case data must be loaded from the database.
                
                // Declare an array to keep the data for each fetched row.
                NSMutableArray *arrDataRow;
                
                // Loop through the results and add them to the results array row by row.
                while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                    // Initialize the mutable array that will contain the data of a fetched row.
                    arrDataRow = [[NSMutableArray alloc] init];
                    
                    // Get the total number of columns.
                    int totalColumns = sqlite3_column_count(compiledStatement);
                    
                    // Go through all columns and fetch each column data.
                    for (int i=0; i<totalColumns; i++){
                        // Convert the column data to text (characters).
                        char *dbDataAsChars = (char *)sqlite3_column_text(compiledStatement, i);
                        
                        // If there are contents in the currenct column (field) then add them to the current row array.
                        if (dbDataAsChars != NULL) {
                            // Convert the characters to string.
                            [arrDataRow addObject:[NSString  stringWithUTF8String:dbDataAsChars]];
                        }
                        
                        // Keep the current column name.
                        if (self.arrColumnNames.count != totalColumns) {
                            dbDataAsChars = (char *)sqlite3_column_name(compiledStatement, i);
                            [self.arrColumnNames addObject:[NSString stringWithUTF8String:dbDataAsChars]];
                        }
                    }
                    
                    // Store each fetched data row in the results array, but first check if there is actually data.
                    if (arrDataRow.count > 0) {
                        [self.arrResults addObject:arrDataRow];
                    }
                }
            }
            else {
                // This is the case of an executable query (insert, update, ...).
                
                // Execute the query.
                int executeQueryResults = sqlite3_step(compiledStatement);
                if (executeQueryResults == SQLITE_DONE) {
                    // Keep the affected rows.
                    self.affectedRows = sqlite3_changes(sqlite3Database);
                    
                    // Keep the last inserted row ID.
                    self.lastInsertedRowID = sqlite3_last_insert_rowid(sqlite3Database);
                }
                else {
                    // If could not execute the query show the error message on the debugger.
                    NSLog(@"DB Error: %s", sqlite3_errmsg(sqlite3Database));
                }
            }
        }
        else {
            // In the database cannot be opened then show the error message on the debugger.
            NSLog(@"%s", sqlite3_errmsg(sqlite3Database));
        }
        
        // Release the compiled statement from memory.
        sqlite3_finalize(compiledStatement);
        
    }
    
    // Close the database.
    sqlite3_close(sqlite3Database);
}

- (NSArray *) loadDataFromDB:(NSString *) query{
    // Run the query and indicate that is not executable.
    // The query string is converted to a char* object.
    [self runQuery:[query UTF8String] isQueryExecutable:NO];
    
    // Returned the loaded results.
    return (NSArray *)self.arrResults;
}

//In this one, there is not a return value.
//However, the affectedRows property can be used to verify whether there were any changes or not after having executed a query.
- (void) executeQuery:(NSString *)query{
    // Run the query and indicate that is executable.
    [self runQuery:[query UTF8String] isQueryExecutable:YES];
}

- (void) saveVideoURL:(int) _idx url:(NSString *) url {
    
    NSArray *existed = [self loadDataFromDB:[NSString stringWithFormat:@"SELECT * FROM URLTABLE WHERE USERID = '%d'", _idx]];
    
    if (existed.count == 0) {
        
        NSString * saveQuery = [NSString stringWithFormat:@"INSERT INTO URLTABLE(USERID, VIDEOURL) VALUES(\"%d\", \"%@\")", _idx, url];
        
        // Execute the query.
        [self executeQuery:saveQuery];
        
    } else {
        
        NSString *updateQuery = [NSString stringWithFormat:@"UPDATE URLTABLE SET VIDEOURL = '%@' WHERE USERID = '%d'", url, _idx];
        
        // Execute the query.
        [self executeQuery:updateQuery];
    }
}

- (NSURL *) getVideoURL:(int) _idx {
    
    NSArray *existed = [self loadDataFromDB:[NSString stringWithFormat:@"SELECT * FROM URLTABLE WHERE USERID = '%d'", _idx]];
    
    if(existed.count > 0) {
        
        NSString *url = [[existed objectAtIndex:0] objectAtIndex:[self.arrColumnNames indexOfObject:@"VIDEOURL"]];
        
        return [NSURL URLWithString:url];
        
    } else {
        
        return nil;
    }
}

- (void) clearDB {
    
    NSString * delQuery = @"DELETE FROM URLTABLE";
    [self executeQuery:delQuery];
}

@end




