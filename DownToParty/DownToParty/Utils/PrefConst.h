//
//  PrefConst.h
//  DChat
//
//  Created by ChenJunLi on 12/17/15.
//  Copyright © 2015 Elite. All rights reserved.
//

// UserDefault Save Key

#ifndef PrefConst_h
#define PrefConst_h

#define PREFKEY_ID              @"user_id"
#define PREFKEY_FULLNAME        @"user_name"
#define PREFKEY_PHOTOURL        @"user_photourl"
#define PREFKEY_LOGGED_IN       @"user_logged_in"

#define PREFKEY_USER_ALLOWPUSH      @"allow_push_setting"
#define PREFKEY_USER_LOCSERVICE     @"allow_loc_service"







#define PREFKEY_EMAIL           @"user_email"
#define PREFKEY_PASSWORD        @"user_password"
#define PREFKEY_AGE             @"user_age"
#define PREFKEY_GENDER          @"user_gender"
#define PREFKEY_SEXUALLITY      @"user_sexuallity"
#define PREFKEY_TAGLINE         @"user_tagline"
#define PREFKEY_INTEREST        @"user_interest"
#define PREFKEY_ABOUTME         @"user_aboutme"

#define PREFKEY_DEVICE_TOKEN    @"device_token"

#define PREFKEY_BADGECOUNT      @"BADGECOUNT"

#endif /* PrefConst_h */
