//
//  ReqConst.h
//  DChat
//
//  Created by ChenJunLi on 12/16/15.
//  Copyright © 2015 Elite. All rights reserved.
//

#ifndef ReqConst_h
#define ReqConst_h

/**
 **     Server URL Macro
 **/
#pragma mark -
#pragma mark - Server URL

#define     SERVER_BASE_URL                 @"http://52.9.22.228"

#define     SERVER_URL [NSString stringWithFormat:@"%@%@", SERVER_BASE_URL, @"/index.php/api/"]

#define     REQ_REGISTER_PHONENUMBER            @"registerPhoneNumber"
#define     REQ_GETAUTHCODE                     @"getAuthCode"
#define     REQ_AUTHCONFIRM                     @"authCodeConfirm"

#define     REQ_PROFILE_PHOTO                   @"setProfile"
#define     REQ_REGISTER                        @"registerUser"





#define REQ_SINGUP                      @"signup"

#define REQ_LOGIN                       @"login"
#define REQ_UPDATEPROFILE               @"updateProfile"

#define REQ_GETDISCOVER                 @"getDiscoverUsers"
#define REQ_GETFLIRTY                   @"getFriends"

#define REQ_UPLOADFILE                  @"uploadFile"
#define REQ_FREINDREQUEST               @"requestFriend"
#define REQ_SETFRIEND                   @"setFriend"
#define REQ_BLOCKFREIND                 @"blockFriend"

#define REQ_UPDATEPROFILE               @"updateProfile"

#define REQ_RESITERTOKEN                @"registerToken"

#define REQ_SETLOCATION                 @"setLocationInfo"

#define REQ_FORGOTPASSWORD              @"forgotPassword"

/**
 **     Response Params
 **/
#pragma mark -
#pragma mark - Response Params

#define RES_CODE                        @"result_code"

#define RES_ID                          @"id"
#define RES_USERINFO                    @"user_info"
#define RES_NAME                        @"name"
#define RES_PHOTOURL                    @"photo_url"







#define RES_EMAIL                       @"email"
#define RES_AGE                         @"age"
#define RES_GENDER                      @"gender"
#define RES_SEXUALLITY                  @"sex"
#define RES_TAGLINE                     @"tag_line"
#define RES_INTEREST                    @"interest"
#define RES_ABOUTME                     @"about_me"


#define RES_ADDRESS                     @"address"
#define RES_FILE_URL                    @"file_url"
#define RES_FILE_COMMENT                @"file_comment"

#define RES_FRIEND_STATE                @"friend_state"

#define RES_DISCOVERUSERS               @"discover_users"
#define RES_FLIRTY                      @"friends"



#define PARAM_ID                        @"id"
#define PARAM_COMMENT                   @"comment"
#define PARAM_FILE                      @"file"


/**
 **     Response Code
 **/

#pragma mark -
#pragma mark - Response Code

#define CODE_SUCCESS                    0
#define CODE_EXISTEMAIL                 101
#define CODE_UNREGUSER                  102
#define CODE_WRONGPWD                   103
#define CODE_NOTEMAIL                   107
#define CODE_WRONGAUTH                  108
#define CODE_WRONEMAIL                  109


#endif /* ReqConst_h */



