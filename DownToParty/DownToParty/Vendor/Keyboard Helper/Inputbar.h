//
//  Inputbar.h
//  Whatsapp
//
//  Created by Rafael Castro on 7/11/15.
//  Copyright (c) 2015 HummingBird. All rights reserved.
//

#import <UIKit/UIKit.h>


//
// Thanks for HansPinckaers for creating an amazing
// Growing UITextView. This class just add design and
// notifications to uitoobar be similar to whatsapp
// inputbar.
//
// https://github.com/HansPinckaers/GrowingTextView
//

@protocol InputbarDelegate;

@interface Inputbar : UIToolbar

@property (nonatomic, assign) id<InputbarDelegate> delegate;
@property (nonatomic) NSString *placeholder;
@property (nonatomic) UIImage *voiceButtonImage;
@property (nonatomic) UIImage *leftButtonImage;
@property (nonatomic) UIImage *rightButtonImage;
@property (nonatomic) NSString *rightButtonText;
@property (nonatomic) UIColor  *rightButtonTextColor;



-(void)resignFirstResponder;
-(NSString *)text;

// disable left, right button, text input field
- (void) setEditable:(BOOL) isEditable;

// disable left button. (only one)
- (void) setSendState:(BOOL) sendState;

@end

@protocol InputbarDelegate <NSObject>
-(void)inputbarDidPressRightButton:(Inputbar *)inputbar;
-(void)inputbarDidPressLeftButton:(Inputbar *)inputbar;
@optional
-(void)inputbarDidChangeHeight:(CGFloat)new_height;
-(void)inputbarDidBecomeFirstResponder:(Inputbar *)inputbar;
@end



