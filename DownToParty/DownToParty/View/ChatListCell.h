//
//  ChatListCell.h
//  DownToParty
//
//  Created by Victory on 4/10/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RoomEntity.h"

@interface ChatListCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIView *vAvatar;
@property (nonatomic, weak) IBOutlet UILabel *lblParticipantsName;
@property (nonatomic, weak) IBOutlet UILabel *lblParticipants;
@property (nonatomic, weak) IBOutlet UILabel *lblRecent;
@property (nonatomic, weak) IBOutlet UIView * vBadge;

- (void) setRoom: (RoomEntity *) chatRoom;

@end
