//
//  ChatListCell.m
//  DownToParty
//
//  Created by Victory on 4/10/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "ChatListCell.h"
#import "M13BadgeView.h"
#import <QuartzCore/QuartzCore.h>
#import "SCGridView.h"
#import "ViewUtils.h"

@interface ChatListCell() {

    M13BadgeView *badgeView;
}

@end

@implementation ChatListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.separatorInset = UIEdgeInsetsMake(0, 75, 0, 10);
    
    // badge view
    badgeView = [[M13BadgeView alloc] initWithFrame:CGRectMake(0, 0, 18.0, 18.0)];
    badgeView.text = [NSString stringWithFormat:@"%d", 0];
    badgeView.hidesWhenZero = YES;
    badgeView.font = [UIFont systemFontOfSize:13.0];
    badgeView.verticalAlignment = M13BadgeViewVerticalAlignmentMiddle;
    badgeView.horizontalAlignment = M13BadgeViewHorizontalAlignmentCenter;
    [self.vBadge addSubview:badgeView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setRoom: (RoomEntity *) chatRoom {
    
    _lblParticipantsName.text = chatRoom._displayName;
    _lblParticipants.text = [NSString stringWithFormat:@"(%d)", chatRoom._currentUsers];
    
    badgeView.text = [NSString stringWithFormat:@"%d", chatRoom._recentCounter];
    
    if (chatRoom._currentUsers <= 2) {
        
        UIImageView *imvAvatar = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.vAvatar.bounds.size.width, self.vAvatar.bounds.size.height)];
        
        imvAvatar.image = [UIImage imageNamed:[NSString stringWithFormat:@"test%d.png", arc4random() %2 + 1]];
        imvAvatar.contentMode = UIViewContentModeScaleAspectFill;
        [self.vAvatar addSubview:imvAvatar];
        
    } else {
    
        // grid layout of participants (maximum users - 4)
        SCGridView *grid = [[SCGridView alloc] initWithFrame:CGRectMake(3, 3, self.vAvatar.bounds.size.width-6, self.vAvatar.bounds.size.height-6)];
        grid.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        grid.layer.cornerRadius = 3.0f;
        grid.clipsToBounds = YES;
        if(chatRoom._currentUsers == 3) {
            
            grid.schema = @[@(1), @(2)];
        } else {
            
            grid.schema = @[@(2), @(2)];
        }
        grid.rowSpacing = 3.0f;
        grid.colSpacing = 3.0f;
        [self.vAvatar addSubview:grid];
        
        NSMutableArray *cells = [[NSMutableArray alloc] initWithCapacity:grid.gridSize];
        
        int nAvatarCount = chatRoom._currentUsers;
        if(nAvatarCount > 4)
            nAvatarCount = 4;
        
        if(nAvatarCount == 3) {
            
            for (int i = 0; i < nAvatarCount; i ++) {
                
                if (i == 0) {
                    
                    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.vAvatar.bounds.size.width-6, (self.vAvatar.bounds.size.height - 9.0)/2.0f)];
                    bgView.backgroundColor= [UIColor clearColor];
                    
                    CGFloat width = floorf((self.vAvatar.bounds.size.width-9.0f)/2.0f);
                    
                    UIImageView *imvAvatar = [[UIImageView alloc] initWithFrame:CGRectMake(floorf((bgView.bounds.size.width - width)/2.0f), 0, (self.vAvatar.bounds.size.width-9.0f)/2.0f, bgView.bounds.size.height)];
                    imvAvatar.contentMode = UIViewContentModeScaleToFill;
                    imvAvatar.image = [UIImage imageNamed:[NSString stringWithFormat:@"test%d.png", arc4random() %2 + 1]];
                    
                    [bgView addSubview:imvAvatar];
                    [cells addObject:bgView];
                    
                } else {
                    UIImageView *imvAvatar = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, (self.vAvatar.bounds.size.width-9.0f)/2.0f, (self.vAvatar.bounds.size.height-9.0f)/2.0f)];
                    imvAvatar.contentMode = UIViewContentModeScaleToFill;
                    imvAvatar.image = [UIImage imageNamed:[NSString stringWithFormat:@"test%d.png", arc4random() %2 + 1]];
                    
                    [cells addObject:imvAvatar];
                }                
            }
            
        } else {
            
            for (int i = 0; i < nAvatarCount; i ++) {
                
                UIImageView *imvAvatar = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, (self.vAvatar.bounds.size.width-9.0f)/2.0f, (self.vAvatar.bounds.size.height-9.0f)/2.0f)];
                imvAvatar.contentMode = UIViewContentModeScaleToFill;
                imvAvatar.image = [UIImage imageNamed:[NSString stringWithFormat:@"test%d.png", arc4random() %2 + 1]];
                
                [cells addObject:imvAvatar];
            }
        }
        
        
        
        
        
        grid.cells = cells;
    }
}

@end
