//
//  FriendListCell.h
//  DownToParty
//
//  Created by victory on 4/9/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendEntity.h"

@interface FriendListCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *imvProfile;
@property (nonatomic, weak) IBOutlet UILabel *lblName;
@property (nonatomic, weak) IBOutlet UILabel *lblState;

- (void) setFriend: (FriendEntity *) _friend;

@end
