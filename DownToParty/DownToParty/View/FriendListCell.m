//
//  FriendListCell.m
//  DownToParty
//
//  Created by victory on 4/9/16.
//  Copyright © 2016 victory. All rights reserved.
//

#import "FriendListCell.h"
#import "CommonUtils.h"

@implementation FriendListCell

- (void)awakeFromNib {
    // Initialization code
    
    self.separatorInset = UIEdgeInsetsMake(0, 70, 0, 10);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setFriend:(FriendEntity *)_friend {

    _lblName.text = _friend._name;
    
    
    if(_friend._state == DOWN_TO_CHILL) {
        
        _lblState.text = @"Down to chill.";
        _lblState.backgroundColor = SelfColor(220, 189, 34);
        
    } else {
        
        _lblState.text = @"Down to party!";
        _lblState.backgroundColor = SelfColor(188, 117, 136);
    }
}

@end
